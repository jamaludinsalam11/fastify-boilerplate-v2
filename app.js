import { fastify } from "fastify"
import AutoLoad from "fastify-autoload"
import dotenv from 'dotenv'

import { dirname, join } from "path"
import { fileURLToPath } from "url"
import { logger } from './plugins/logger'

const __filename    = fileURLToPath(import.meta.url)
const __dirname     = dirname(__filename)

const app           = fastify({logger: logger})


    /** Security */
    // fastify.register(import('./plugins/rate-limit.js'))
    /** Initilize Env */
    dotenv.config()
    /** Initilize DB */
    app.register(import('./config/db.js'))
    /** Initilize Plugin */
    app.register(import('./plugins/index.js'))
    /** Documenting API */
    app.register(import('./plugins/swagger.js'))
    
    /** Initilize Routes /  Services */
    app.register(AutoLoad, {
        dir: join(__dirname, "routes"),
        options: Object.assign({}, app.options)
    }) 
    
    /** 
     * RUN server with choiced PORT  
     * -----------------------------
     * If code listen down below removed, 
     * By Default Fastify run on port 3000
     * */
    const start = async() => {
        try {
            await app.listen(process.env.PORT)
        } catch (err) {
            app.log.error(err)
            process.exit(1)
        }
    }
    start()



import fp from 'fastify-plugin'
import mysql from 'mysql2'
/** 
 * Connect Database as Fastify Plugin
 */

async function dbConnect(fastify, options, done){
    const env = process.env
    fastify.register(import('fastify-mariadb'),{
        promise: true,
        connectionString: `mariadb://${env.DB_USER}:${env.DB_PASS}@${env.DB_HOST}:${env.DB_PORT}/${env.DB_NAME}`,
         
    })
    
    done()
}

// async function dbMysql(fastify, opts, done){
//     mysql.createPool({
//         "host": process.env.DB_HOST,
//         "user": process.env.DB_USER,
//         "password": process.env.DB_PASS,
//         "database": process.env.DB_NAME,
//         "port": process.env.DB_PORT
//     })
// }

export default fp(dbConnect)

const postOpts = {
    schema: {
        body: {
            type: "object",
            required: ["genre"],
            genre: {
                type: "string"
            }

        },
        response: {
            200: {
                type: "array",
                items: {
                    type: "object",
                    properties: {
                        genre: {
                            type: "string"
                        }
                    }
                }
            }
        }
        
    }
}


export default async function(fastify, opts, done){
    // const genres = fastify.music()
    fastify.get('/', async function(request, reply) {
        return "hello world"
    })
    fastify.route({
        method: 'GET',
        url: '/try',
        schema: {
            response: {
                200: {
                    type: 'object',
                    properties: {
                        root: { type: 'string'}
                    }
                }
            }
        },
        handler: async(request, reply)=> {
            return {root: 'coba dulu'}
        }
    })
    // fastify.get('/test', async function(request, reply) {
    //     const connection = await fastify.mariadb.getConnection()
    //     let sql = `
    //         select * from people
    //     `
    //     connection.release()
    //     const result = await fastify.mariadb.query(sql)
    //     return result
    // })
    // fastify.get('/:id', async function(request, reply) {
    //    try{
    //         const genre = genres.find(genre => genre.id === +request.params.id)
    //         return genre
    //    } catch (err){
    //         reply.code(404).send("Not Found")
    //    }
    // })

    // fastify.post('/', postOpts, async function(request, reply) {
    //     const { genre } = request.body
    //     if(!genre){
    //         reply.code(404).send("Not Found")
    //     }
    //     const listOfGenres = fastify.music(genre)
    //     return listOfGenres
    // })
    
    done()
}
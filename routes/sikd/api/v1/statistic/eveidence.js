export default async function(fastify, opts, done){
    fastify.get('/eveidence', async function(request, reply) {
        let sql = `
            SELECT NId,GIR_Id,  RoleId_From, RoleId_To, From_Id, To_Id, StatusReceive, ReceiveDate, readDate, differences, 
            CONCAT (FLOOR(differences / 3600 / 24), ' Hari ' , FLOOR(hours_part / 3600),' Jam ', FLOOR(minutes_part / 60), ' Menit') AS difference 
            FROM (SELECT MOD(differences, 60) AS seconds_part, MOD(differences, 3600) AS minutes_part, MOD(differences, 3600 * 24) AS hours_part, 
            NId, GIR_Id, RoleId_From, RoleId_To, From_Id,To_Id,
            Hal, ReceiveDate,StatusReceive, readDate  , differences
            FROM  (SELECT  i.NId AS NId, rr.GIR_Id AS GIR_Id, rr.StatusReceive AS StatusReceive,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
                      Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To, rr.RoleId_From AS RoleId_From, rr.From_Id AS From_Id, rr.To_Id AS To_Id,
                      i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz ,
                      IF(rr.readDate IS NULL AND rr.StatusReceive = 'unread' AND rr.ReceiveDate > '2021-02-29 23:59:59' , CURRENT_TIMESTAMP, rr.readDate) AS readDate, 
                      TIMESTAMPDIFF(SECOND,rr.ReceiveDate,
                  IF(rr.readDate IS NULL AND rr.StatusReceive = 'unread' AND rr.ReceiveDate > '2021-02-25 23:59:59' , CURRENT_TIMESTAMP, rr.readDate) )AS differences 
                      FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)  
                      AND (rr.NId = i.NId)))) 
                      JOIN people p ON p.PeopleId  = rr.To_Id AND p.PeopleIsActive = '1'
                      JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
                      AND (rr.RoleId_To LIKE "uk.1.1.18%" ) AND rr.StatusReceive LIKE '%read%' AND (rr.ReceiveDate BETWEEN "2021-01-01 00:00:00" AND "2021-08-30 23:59:59")  
                      ORDER BY ReceiveDate DESC) a) b
        `

        const connection = await fastify.mariadb.getConnection()
        connection.release()
        const result = await fastify.mariadb.query(sql)
        return result
    })

    done()
    
}
const schema = {
    schema: {
        body: {
            type: "object",
            properties: {
                PeopleId: {
                    type: "number"
                },
                PeopleName: {
                    type: "string"
                }
            }

        },
        response: {
            200: {
                type: "array",
                items: {
                    type: "object",
                    properties: {
                        PeopleId: {
                            type: "number"
                        },
                        PeopleName: {
                            type: "string"
                        }
                    }
                }
            }
        }
        
    }
}
// export default async function(fastify, opts, done){
   
//     fastify.get('/total_notadinas', schema, async function(request, reply) {
//         const {PeopleId} = request.params
//         let sql = `
//             select * from people 
//         `
//         fastify.mariadb.query(sql, (err, result) => {
//             return (err || result)
//         })

//         // let results = []
//         // let dataObj={}
//         // for(let i = 0; i < result22.length; i++){
//         //     const obj = result22[i]

//         //     dataObj = {
//         //         'PeopleId' : obj.PeopleId,
//         //         'PeopleName': obj.PeopleName
//         //     }
//         //     results[i] = dataObj
//         // }
//         // return result22
//     })
// }
export default async function(fastify, opts, done){
    console.log('debug fastify',)
    fastify.get('/total_notadinas/:PeopleId', schema, async function(request, reply) {
        const {PeopleId} = request.params
        const connection = await fastify.mariadb.getConnection()
        let sql = `
            select * from people where PeopleId = '${PeopleId}'
        `
        connection.release()
        const result = await fastify.mariadb.query(sql)
        
        let results = []
        let dataObj={}
        for(let i = 0; i < result.length; i++){
            const obj = result[i]

            dataObj = {
                'PeopleId' : obj.PeopleId,
                'PeopleName': obj.PeopleName
            }
            results[i] = dataObj
        }
        return results
    })
    done()
}
// import {version} from '../../package.json'
import { createRequire } from "module"; // Bring in the ability to create the 'require' method
const requires = createRequire(import.meta.url); // construct the require method
const jsonfile = requires('../../../../package.json')

export default async function(fastify, opts, done){
    fastify.get('/statistic', async function(request, reply) {
        return{
            api: 'MKRI',
            engine: `nodejs - fastify`,
            version: `${jsonfile.version}`,
            status: 'Healthy'
        }
    })
    done()
}
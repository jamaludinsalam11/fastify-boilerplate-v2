import fp from 'fastify-plugin'

// export default swaggerOpt
async function swagger(fastify, opts, done){
    fastify.register(import('fastify-swagger'), {
        routePrefix: '/api-docs',
        swagger: {
            info: {
                title: 'MKRI API',
                description: 'REST API with Node.js, Fastify, and MariaDB',
                version: '1.0.0'
            },
            externalDocs: {
                url: 'https://swagger.io',
                description: 'Find more info here'
            },
            host: 'localhost:3000',
            schemes: ['http', 'https'],
            consumes: ['application/json'],
            produces: ['application/json'],
            
        },
        exposeRoute: true
    })

    done()
}
export default fp(swagger)






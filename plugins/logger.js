import pino from 'pino'
import pretty from 'pino-pretty'

export const logger = pino({
    prettyPrint: {
        colorize: true,
        levelFirst: true,
        ignore: 'reqId, req'
    },
    serializer: {
        res (reply) {
            return {
                statusCode : reply.statusCode,
                responseTime: reply.responseTime
            }
        },
        req (request){
            return {
                method: request.method
            }
        }
    },
    prettifier: pretty
})

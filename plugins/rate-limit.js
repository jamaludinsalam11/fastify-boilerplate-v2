import fp from 'fastify-plugin'

async function rateLimit(fastify, opts, done){
    fastify.register(import('fastify-rate-limit'), {
        max: 3,
        timeWindows: '60 minute'
    })

    done()
}

export default fp(rateLimit)
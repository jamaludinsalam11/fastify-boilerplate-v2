import Fastify from "fastify"
import AutoLoad from "fastify-autoload"
import dotenv from 'dotenv'

import { dirname, join } from "path"
import { fileURLToPath } from "url"

const __filename    = fileURLToPath(import.meta.url)
const __dirname     = dirname(__filename)
const fastify       = Fastify({logger: true})

export default async function (fastify, opts){
    /** Security */
    // fastify.register(import('./plugins/rate-limit.js'))
    /** Initilize Env */
    dotenv.config()
    /** Initilize DB */
    fastify.register(import('./config/db.js'))
    /** Initilize Plugin */
    fastify.register(import('./plugins/index.js'))
    /** Documenting API */
    fastify.register(import('./plugins/swagger.js'))
    
    /** Initilize Routes /  Services */
    fastify.register(AutoLoad, {
        dir: join(__dirname, "routes"),
        options: Object.assign({}, opts)
    }) 
    
    /** 
     * RUN server with choiced PORT  
     * -----------------------------
     * If code listen down below removed, 
     * By Default Fastify run on port 3000
     * */
    try{
        fastify.listen(process.env.PORT, (err, address) => {
          
            // if(err){
            //     fastify.log.error('ROOT ERROR:',err)
            // }
            fastify.swagger()
            fastify.log.info(`Server running as well on ${address}`)
        })

    } catch (err){
        fastify.log.error(`Error on : ${err}`)
    }

}